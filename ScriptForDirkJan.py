#This script is made by Giorgio Oliveri PhD student @ AMOLF. oliveri@amolf.nl

# This PYTHON script saves the coordinates of a curve defined by a paramater c1_geom
# The number of points to define the curve is set to npoint=100, but can be changed 
# (preferably not). When this script is run a txt file is created with the coordinates
# of the npoints in the format [x y z] where z is always 0. A plot of points coordinates
# is also saved.
import numpy as np
import matplotlib.pyplot as plt

## Some Gemtetrical parameters to keep constant
Phi = 0.5;

L=15;
D =np.sqrt(L**2*4/(np.pi*Phi));
# this is the parameter which changes the curve, you can play a bit around with it. 
# when runnign the script a 
c1_geom = 0.1; ##

npoints = 100;
r0 = L*np.sqrt(2*Phi)/np.sqrt((np.pi*(2+c1_geom**2)));
theta = np.linspace(0, 2*np.pi, npoints);
rtheta = r0*(1+c1_geom*np.cos(4*theta));
x=rtheta*np.cos(theta);
y=rtheta*np.sin(theta);
z = x*0.0;

plt.scatter(x, y)
plt.scatter([-L/2, -L/2, L/2, L/2], [-L/2, L/2, L/2, -L/2])
plt.absolute_import
plt.axes().set_aspect('equal')
plt.title('c1 = '+str(c1_geom))
plt.savefig('NodesPositions_c1='+str(c1_geom)+'.png', dpi=ImageRes, figsize=(5,5))


txtName = 'CoordinatesList_c1='+str(c1_geom)+'.txt'


np.savetxt(txtName, np.c_[x,y,z])




