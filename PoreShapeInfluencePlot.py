# Script made by Giorgio Oliveri g.oliveri@amolf.nl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import glob 
import os.path
import colormaps as cmaps
from mpl_toolkits.axes_grid1 import make_axes_locatable

#matplotlib inline

MainDirectory = 'D:\\My Documents\\ABAQUS\\PoreShapeInfluence_NonSelfAdjointStudy/'
SubFolderName = 'FinerMesh_GroupedOutputFiles'
FileName = 'Eignevalue_Step2.txt'

c1Range = np.linspace(-0.1, 0.1, 101)
nmodes = 20
ImageRes = 300
fontsize = 15
PlotsDirectory = MainDirectory+'Plots/'
PlotON = True
SavePlotON = True



if os.path.isdir(PlotsDirectory)==False:
    os.mkdir(PlotsDirectory)


eigen= np.zeros([c1Range.shape[0],nmodes ])




for index, c1 in enumerate(c1Range):
    FullDirectoryRef = MainDirectory+SubFolderName+'/FinerMesh_Holey_Beam_c1='+str(round(c1,4))+'/';
    eigen[index, :] = np.loadtxt(FullDirectoryRef+FileName)[:, 2]
    
   
    
    
#%%
fig, ax1 = plt.subplots()
plt.title('ABQ 1x10 u.c., >100elem TRI CPE8 per u.c. size ')
ax1.set_xlabel('c1');
ax1.set_ylabel('Eigenvalues $\lambda$ [-]');
modesToPlot = 20;

cmap = plt.get_cmap('viridis',modesToPlot)
colors = plt.cm.viridis(np.linspace(0, 1, 20))
ax1.set_prop_cycle('color', colors)
ax1.plot(c1Range, eigen[:, 0:modesToPlot])
plt.ylim(0.25, 0.8)

norm = mpl.colors.Normalize(vmin=1,vmax=20)
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])
plt.colorbar(sm, ticks=np.linspace(0,modesToPlot, 11), 
             boundaries=np.arange(1,21,1), label = 'Mode Number [-]', orientation= 'vertical');
#plt.tight_layout()
if SavePlotON:
    plt.savefig(PlotsDirectory+'/PoreShapeInfluencePlot_'+str(modesToPlot)+'modes_GradualShading.png', dpi=ImageRes, figsize=(5,5))

#%%
fig, ax1 = plt.subplots()
plt.title('ABQ 1x10 u.c., >100elem TRI CPE8 per u.c. size ')
ax1.set_xlabel('c1');
ax1.set_ylabel('Eigenvalues $\lambda$ [-]');
modesToPlot = 20;

ax1.plot(c1Range, eigen[:, 0:modesToPlot])
plt.ylim(0.25, 0.8)

plt.tight_layout()

if SavePlotON:
    plt.savefig(PlotsDirectory+'/PoreShapeInfluencePlot_'+str(modesToPlot)+'modes.png', dpi=ImageRes, figsize=(5,5))