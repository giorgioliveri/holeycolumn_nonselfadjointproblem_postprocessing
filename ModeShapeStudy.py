# Script made by Giorgio Oliveri g.oliveri@amolf.nl
import numpy as np
import matplotlib.pyplot as plt
import glob 
import os.path
#matplotlib inline

MainDirectory = 'D:\\My Documents\\ABAQUS\\PoreShapeInfluence_NonSelfAdjointStudy/'
SubFolderName = 'FinerMesh_GroupedOutputFiles'
FileName = 'Eigenvectors_Step2.txt'

c1Range = np.linspace(-0.1, 0.1, 101)
nmodes = 20
ImageRes = 300
fontsize = 15
PlotsDirectory = MainDirectory+'Plots/'
PlotON = True
SavePlotON = False



if os.path.isdir(PlotsDirectory)==False:
    os.mkdir(PlotsDirectory)

index = 0;
c1 = c1Range[0];
FullDirectoryRef = MainDirectory+SubFolderName+'/FinerMesh_Holey_Beam_c1='+str(round(c1,4))+'/';
Nodes = np.unique(np.loadtxt(FullDirectoryRef+FileName)[:, 1]);
Frames = np.unique(np.loadtxt(FullDirectoryRef+FileName)[:, 0]);
nNodes = Nodes.shape[0];
nFrames = Frames.shape[0];

frame= np.zeros([c1Range.shape[0],nmodes ])
x = np.zeros([nNodes*nFrames, c1Range.shape[0]]);
y = np.zeros([nNodes*nFrames, c1Range.shape[0]]);
dx = np.zeros([nNodes*nFrames, c1Range.shape[0]])
dy = np.zeros([nNodes*nFrames, c1Range.shape[0]])
eigen= np.zeros([c1Range.shape[0],nmodes ])



for index, c1 in enumerate(c1Range):
    FullDirectoryRef = MainDirectory+SubFolderName+'/FinerMesh_Holey_Beam_c1='+str(round(c1,4))+'/';
    x[:, index] = np.loadtxt(FullDirectoryRef+FileName)[:, 2]
    y[:, index] = np.loadtxt(FullDirectoryRef+FileName)[:, 3]
    dx[:, index] = np.loadtxt(FullDirectoryRef+FileName)[:, 5]
    dy[:, index] = np.loadtxt(FullDirectoryRef+FileName)[:, 6]
    eigen[index, :] = np.loadtxt(FullDirectoryRef+ 'Eignevalue_Step2.txt')[:, 2]
    
dx = dx/300;
dy = dy/300;
    
#%% plot modeshape
modeToPlot = 1;
c1_value = -0.1;

c1Index = np.where(abs(c1_value-c1Range)<1e-4)
min_index = 0+modeToPlot*nNodes;
max_index = min_index+(nNodes);
plt.scatter(x[min_index:max_index, c1Index[0][0]],y[min_index:max_index,c1Index[0][0]])
plt.scatter(x[min_index:max_index, c1Index[0][0]]+dx[min_index:max_index, c1Index[0][0]],y[min_index:max_index,c1Index[0][0]]+dy[min_index:max_index, c1Index[0][0]])
plt.title('ModeShape_n_'+str(modeToPlot)+'_c1='+str(c1Range[c1Index]))
if SavePlotON:
    plt.savefig(PlotsDirectory+'/ModeShape_n_'+str(modeToPlot)+'_c1='+str(c1Range[c1Index])+'.png', dpi=ImageRes, figsize=(5,5))
#%%    
modeToPlot = 1;
totModeToConsider = 20
#min_index = 0+modeToPlot*nNodes;
#max_index = min_index+(nNodes);
#shape1_x = (x[min_index:max_index, c1Index]+dx[min_index:max_index, c1Index]);
#
#modeToPlot = 3;
#min_index = 0+modeToPlot*nNodes;
#max_index = min_index+(nNodes);
#
#shape2_x = (x[min_index:max_index, c1Index]+dx[min_index:max_index, c1Index]);
#
#print(np.corrcoef(shape1_x,shape1_x))
#
#print(np.corrcoef(shape1_x,shape2_x))

prova = np.zeros([22,totModeToConsider])
for i in range(0,totModeToConsider):
    modeToPlot = i+1;
    min_index = 0+modeToPlot*nNodes;
    max_index = min_index+(nNodes);
    prova[:,i]= np.append(dx[min_index:max_index, c1Index[0][0]],dy[min_index:max_index, c1Index[0][0]]) ;

#CoeffCorr = np.corrcoef(prova;)
#%%


plt.imshow(np.abs(np.corrcoef(prova, rowvar = False))); 
plt.colorbar()     
if SavePlotON:
    plt.savefig(PlotsDirectory+'/CorrCoeff'+'_c1='+str(c1Range[c1Index])+'.png', dpi=ImageRes, figsize=(5,5))      


#%%
mode1 = 8;
mode2 = 19;

min_index_m1 = 0+mode1*nNodes;
max_index_m1 = min_index_m1+(nNodes);

min_index_m2 = 0+mode2*nNodes;
max_index_m2 = min_index_m2+(nNodes);
    
plt.scatter(np.append(dx[min_index_m1:max_index_m1, c1Index[0][0]], dy[min_index_m1:max_index_m1, c1Index[0][0]] ), np.append(dx[min_index_m2:max_index_m2, c1Index[0][0]], dy[min_index_m2:max_index_m2, c1Index[0][0]] )        )
    
#%% compare same mode different c1

ModeToConsider = 3l;
temp = np.zeros([22, c1Range.shape[0]])

min_index = 0+ModeToConsider*nNodes;
max_index = min_index+(nNodes);

for i in np.arange(c1Range.shape[0]):
    temp[:,i] = np.append(dx[min_index:max_index, i], dy[min_index:max_index, i])
    
matrix  =np.abs(np.corrcoef(temp, rowvar = False));
plt.imshow(matrix)
plt.colorbar()
dia = np.diagonal(matrix, offset=1)
swapIndeces = np.where(dia<0.8)
swapIndeces = np.array(swapIndeces)
zoomarea = [85, 90]
#plt.xlim(zoomarea)
#plt.ylim([zoomarea[1], zoomarea[0]])

print([swapIndeces])

#%% Swap Eigenvalues
swapIndeces_matrix = np.zeros([20,10])

for mode in np.arange(1,nmodes):
    ModeToConsider = mode;
    temp = np.zeros([22, c1Range.shape[0]])
    min_index = 0+ModeToConsider*nNodes;
    max_index = min_index+(nNodes);
    for i in np.arange(c1Range.shape[0]):
        temp[:,i] = np.append(dx[min_index:max_index, i], dy[min_index:max_index, i])
    
    matrix  =np.abs(np.corrcoef(temp, rowvar = False));
    dia = np.diagonal(matrix, offset=1)
    if len(np.where(dia<0.8)[0])>0:
        temp_index =  np.where(dia<0.8)[0][0]+1
    
        dx[min_index:max_index, temp_index:], dx[min_index+11:max_index+11, temp_index:] = dx[min_index+11:max_index+11, temp_index:], dx[min_index:max_index, temp_index:]
        dy[min_index:max_index, temp_index:], dy[min_index+11:max_index+11, temp_index:] = dy[min_index+11:max_index+11, temp_index:], dy[min_index:max_index, temp_index:]
    
    
        eigen[temp_index: ,ModeToConsider-1],  eigen[temp_index: ,ModeToConsider] = eigen[temp_index: ,ModeToConsider], eigen[temp_index: ,ModeToConsider-1].copy()

    
    # the problem is here to swap the eigenvalues

#%%
fig, ax1 = plt.subplots()
plt.title('ABQ 1x10 u.c., >100elem TRI CPE8 per u.c. size ')
ax1.set_xlabel('c1');
ax1.set_ylabel('Eigenvalues $\lambda$ [-]');
modesToPlot = 20;

ax1.plot(c1Range, eigen[:, 0:modesToPlot])
plt.ylim(0.25, 0.8)

plt.tight_layout()

if SavePlotON:
    plt.savefig(PlotsDirectory+'/PoreShapeInfluencePlot_'+str(modesToPlot)+'modes.png', dpi=ImageRes, figsize=(5,5))    
    
